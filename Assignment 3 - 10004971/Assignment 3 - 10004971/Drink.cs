﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3___10004971
{
    class Drink
    {
        string name;
        int price;
        static List<Tuple<string, int>> drinks = new List<Tuple<string, int>>();

        //Sets up the menu the user can choose from
        static Drink()
        {
            drinks.Add(Tuple.Create("Pepsi", 3));
            drinks.Add(Tuple.Create("Coke", 4));
            drinks.Add(Tuple.Create("Juice", 2));
            drinks.Add(Tuple.Create("Sprite", 3));
            drinks.Add(Tuple.Create("Fanta", 3));
        }

        //Returns the name of the drink
        public string DrinkName
        {
            get
            {
                return name;
            }
        }

        //Returns the price of the drink
        public int DrinkPrice
        {
            get
            {
                return price;
            }
        }

        //Allows the user to choose their drink
        public void ChooseDrink()
        {
            int selection;

            Console.WriteLine("******************************************************");
            Console.WriteLine("Please enter the number (1-5) of your choice of drink.");
            for (int i = 0; i < drinks.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {drinks[i].Item1} - ${drinks[i].Item2}");
            }
            Console.WriteLine("******************************************************");
            if (int.TryParse(Console.ReadLine(), out selection) && selection > 0 && selection < drinks.Count + 1)
            {
                name = drinks[selection-1].Item1;
                price = drinks[selection-1].Item2;
                Console.Clear();
                Console.WriteLine($"'{name}' has been added to your order.");
                Console.WriteLine();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that was an invalid selection.");
                Console.WriteLine();
                ChooseDrink();
            }   
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3___10004971
{
    class Food
    {
        static List<Tuple<string, int>> toppingsMenu = new List<Tuple<string, int>>();
        static List<Tuple<string, int, string>> pizzaMenu = new List<Tuple<string, int, string>>();
        Dictionary<string, string> pizzaDetails = new Dictionary<string, string>();
        List<Tuple<string, int>> pizzaToppings = new List<Tuple<string, int>>();

        //Sets up the various menu's the user can choose from
        static Food()
        {
            pizzaMenu.Add(Tuple.Create("Cheese", 0, "No toppings, just cheese."));
            pizzaMenu.Add(Tuple.Create("Ham", 3, "Ham."));
            pizzaMenu.Add(Tuple.Create("Pepperoni", 3, "Pepperoni."));
            pizzaMenu.Add(Tuple.Create("Seafood", 3, "Prawns."));
            pizzaMenu.Add(Tuple.Create("Vegetarian", 4, "Capsicum, pineapple"));
            pizzaMenu.Add(Tuple.Create("Hawaiian", 5, "Pineapple, ham."));
            pizzaMenu.Add(Tuple.Create("Chicken Supreme", 5, "Chicken, mushrooms."));
            pizzaMenu.Add(Tuple.Create("Meatlovers", 9, "Beef, bacon, chicken."));
            pizzaMenu.Add(Tuple.Create("Deluxe", 9, "Chicken, mushrooms, capsicum, pineapple."));
            pizzaMenu.Add(Tuple.Create("Extreme", 10, "Jalapeno, capsicum, pepperoni, bacon."));

            toppingsMenu.Add(Tuple.Create("Barbeque Swirl", 0));
            toppingsMenu.Add(Tuple.Create("Aioli Swirl", 0));
            toppingsMenu.Add(Tuple.Create("Garlic Sprinkle", 0));
            toppingsMenu.Add(Tuple.Create("Onion", 1));
            toppingsMenu.Add(Tuple.Create("Tomato", 1));
            toppingsMenu.Add(Tuple.Create("Sweetcorn", 1));
            toppingsMenu.Add(Tuple.Create("Capsicum", 2));
            toppingsMenu.Add(Tuple.Create("Mushrooms", 2));
            toppingsMenu.Add(Tuple.Create("Pineapple", 2));
            toppingsMenu.Add(Tuple.Create("Jalapeno", 2));
            toppingsMenu.Add(Tuple.Create("Bacon", 3));
            toppingsMenu.Add(Tuple.Create("Beef", 3));
            toppingsMenu.Add(Tuple.Create("Pepperoni", 3));
            toppingsMenu.Add(Tuple.Create("Ham", 3));
            toppingsMenu.Add(Tuple.Create("Chicken", 3));
            toppingsMenu.Add(Tuple.Create("Prawns", 3));
        }

        //The below code is used for both custom and regular pizzas
        //This method sets up the pizza being selected with default values that will be overwritten
        public Food()
        {
            pizzaDetails.Add("Name", "Pizza");
            pizzaDetails.Add("ToppingsPrice", "0");
            pizzaDetails.Add("BasePrice", "0");
            pizzaDetails.Add("BaseName", "Deep Pan");
            pizzaDetails.Add("Size", "Small");
            pizzaDetails.Add("SizePrice", "0");
            pizzaDetails.Add("Total", "0");
            pizzaDetails.Add("Description", "");
        }

        //This method allows us to change the details entered into the dictionary, at the appropriate times.
        public Dictionary<string, string> ChangeDetails
        {
            set
            {
                pizzaDetails["BaseName"] = value["BaseName"];
                pizzaDetails["Size"] = value["Size"];
                pizzaDetails["BasePrice"] = value["BasePrice"];
                pizzaDetails["SizePrice"] = value["SizePrice"];
                pizzaDetails["Total"] = ((int.Parse(pizzaDetails["ToppingsPrice"])) + (int.Parse(pizzaDetails["BasePrice"])) + (int.Parse(pizzaDetails["SizePrice"]))).ToString();
            }
        }

        //This method allows us to return the pizza details that are stored in the dictionary
        public Dictionary<string, string> ReturnDetails
        {
            get
            {
                return pizzaDetails;
            }
        }

        //The below method is for regular menu pizzas only
        //This method allows the user to select a pre-decided pizza
        public void ChooseMenuPizza()
        {
            int selection;

            Console.WriteLine("************************************************");
            Console.WriteLine("Please enter the number of your choice of toppings.");
            for (int i = 0; i < pizzaMenu.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {pizzaMenu[i].Item1} - ${pizzaMenu[i].Item2}");
                Console.WriteLine($"    * {pizzaMenu[i].Item3}");
            }
            Console.WriteLine("************************************************");
            if (int.TryParse(Console.ReadLine(), out selection) && selection > 0 && selection < pizzaMenu.Count + 1)
            {
                pizzaDetails["ToppingsPrice"] = pizzaMenu[selection - 1].Item2.ToString();
                pizzaDetails["Name"] = pizzaMenu[selection - 1].Item1;
                pizzaDetails["Description"] = pizzaMenu[selection - 1].Item3;
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that was an invalid selection.");
                Console.WriteLine();
                ChooseMenuPizza();
            }
        }

        //The below methods are for custom pizzas only
        //This method allows the user to modify the name of their custom pizza
        public void PizzaName()
        {
            Console.WriteLine("Please enter a name for this custom pizza.");
            pizzaDetails["Name"] = Console.ReadLine();
            if (pizzaDetails["Name"].Any(char.IsDigit) || pizzaDetails["Name"] == "")
            {
                Console.Clear();
                Console.WriteLine("Sorry, that is an invalid selection.");
                Console.WriteLine();
                PizzaName();
            }
            Console.Clear();
        }

        //This method allows the user to select the toppings of their custom pizza
        public void ChooseToppings()
        {
            int selection;

            Console.WriteLine("**************************************************");
            Console.WriteLine("Please enter the number of your choice of topping.");
            for (int i = 0; i < toppingsMenu.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {toppingsMenu[i].Item1} - ${toppingsMenu[i].Item2}");
            }
            Console.WriteLine("**************************************************");
            if (!int.TryParse(Console.ReadLine(), out selection) || selection > toppingsMenu.Count || selection < 1)
            {
                Console.Clear();
                Console.WriteLine("Sorry, that was an invalid selection.");
                Console.WriteLine();
                ChooseToppings();
            }
            else
            {
                pizzaToppings.Add(Tuple.Create(toppingsMenu[selection - 1].Item1, toppingsMenu[selection - 1].Item2));
                pizzaDetails["ToppingsPrice"] = (int.Parse(pizzaDetails["ToppingsPrice"]) + toppingsMenu[selection - 1].Item2).ToString();
                pizzaDetails["Total"] = (int.Parse(pizzaDetails["Total"]) + toppingsMenu[selection - 1].Item2).ToString();
            }
        }

        //This method returns the list of pizza toppings for the custom pizza
        public List<Tuple<string, int>> ReturnPizzaToppings
        {
            get
            {
                return pizzaToppings;
            }
        }
    }
}
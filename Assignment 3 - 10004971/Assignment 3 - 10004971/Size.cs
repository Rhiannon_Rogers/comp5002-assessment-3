﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3___10004971
{
    class Size
    {
        static Dictionary<string, string> details = new Dictionary<string, string>();
        static List<Tuple<string, int>> sizes = new List<Tuple<string, int>>();
        static List<Tuple<string, int>> bases = new List<Tuple<string, int>>();

        //Sets up the various menu's the user can select from
        static Size()
        {
            sizes.Add(Tuple.Create("Kids", 0));
            sizes.Add(Tuple.Create("Small", 1));
            sizes.Add(Tuple.Create("Medium", 3));
            sizes.Add(Tuple.Create("Large", 5));
            sizes.Add(Tuple.Create("Extra Large", 7));
            sizes.Add(Tuple.Create("Party Size", 10));

            bases.Add(Tuple.Create("Thin & Crispy", 5));
            bases.Add(Tuple.Create("Deep Pan", 5));
            bases.Add(Tuple.Create("Gluten-Free", 7));

            details.Add("BasePrice", "0");
            details.Add("BaseName", "Deep Pan");
            details.Add("Size", "Small");
            details.Add("SizePrice", "0");
        }

        //Returns the size and base details for the pizza being selected
        public static Dictionary<string, string> ReturnDetails
        {
            get
            {
                return details;
            }
        }

        //Allows the user to choose the base of the pizza
        public static void ChooseBase(string name, int price)
        {
            int selection;

            Console.WriteLine("**************************************************************");
            Console.WriteLine("Please enter the number of your choice of base for this pizza.");
            for (int i = 0; i < bases.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {name} ({bases[i].Item1}) - ${bases[i].Item2 + price}");
            }
            Console.WriteLine("**************************************************************");
            if (int.TryParse(Console.ReadLine(), out selection) && selection > 0 && selection < bases.Count + 1)
            {
                details["BaseName"] = bases[selection - 1].Item1;
                details["BasePrice"] = (bases[selection - 1].Item2).ToString();
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that was an invalid selection.");
                Console.WriteLine();
                ChooseBase(name, price);
            }
        }

        //Allows the user to choose the size of the pizza
        public static void ChooseSize(string name, int price)
        {
            int selection;

            Console.WriteLine("**************************************************************");
            Console.WriteLine("Please enter the number of your choice of size for this pizza.");
            for (int i = 0; i < sizes.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {name} ({sizes[i].Item1}) - ${sizes[i].Item2 + price}");
            }
            Console.WriteLine("**************************************************************");
            if (int.TryParse(Console.ReadLine(), out selection) && selection > 0 && selection < sizes.Count + 1)
            {
                details["Size"] = sizes[selection - 1].Item1;
                details["SizePrice"] = (sizes[selection - 1].Item2).ToString();
                Console.Clear();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that was an invalid selection.");
                Console.WriteLine();
                ChooseSize(name, price);
            }
        }
    }
}
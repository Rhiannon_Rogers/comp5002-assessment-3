﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3___10004971
{

    class Program
    {
        static List<Food> food = new List<Food>();
        static List<Drink> drinks = new List<Drink>();

        static void Main(string[] args)
        {
            Menu();
        }

        //This method houses the main menu
        static void Menu()
        {
            int value;

            Console.WriteLine("***************************************************************************");
            Console.WriteLine("What would you like to do today? Please enter the number of your selection.");
            Console.WriteLine("1. Add Pizza.");
            Console.WriteLine("2. Add Drink.");
            Console.WriteLine("3. View Order.");
            Console.WriteLine("4. Remove Item From Order.");
            Console.WriteLine("5. Checkout.");
            Console.WriteLine("***************************************************************************");
            if (int.TryParse(Console.ReadLine(), out value))
            {
                switch (value)
                {
                    case 1:
                        Console.Clear();
                        OrderPizza();
                        break;
                    case 2:
                        Console.Clear();
                        OrderDrink();
                        break;
                    case 3:
                        Console.Clear();
                        DisplayOrder();
                        Menu();
                        break;
                    case 4:
                        Console.Clear();
                        DeleteItem();
                        break;
                    case 5:
                        Console.Clear();
                        Checkout();
                        break;
                }
            }
            Console.Clear();
            Console.WriteLine("Sorry, that is an invalid selection.");
            Console.WriteLine();
            Menu();
        }

        //This method allows the user to order a drink
        static void OrderDrink()
        {
            drinks.Add(new Drink());
            drinks[drinks.Count - 1].ChooseDrink();
            Menu();
        }

        //This method allows the user to order a pizza
        static void OrderPizza()
        {
            Console.Clear();
            food.Add(new Food());
            Console.WriteLine("Add custom pizza? Type 'Y' or 'Yes' for yes, or press any key for no.");
            string answer = Console.ReadLine().ToUpper();
            if (answer == "YES" || answer == "Y")
            {
                Console.Clear();
                food[food.Count - 1].PizzaName();
                ChooseSize();

            Topping:

                ViewFood();
                food[food.Count - 1].ChooseToppings();
                Console.Clear();
                Console.WriteLine($"'{food[food.Count - 1].ReturnPizzaToppings[food[food.Count - 1].ReturnPizzaToppings.Count - 1].Item1}' has been added to {food[food.Count-1].ReturnDetails["Name"]}.");
                Console.WriteLine("");
                ViewFood();
                Console.WriteLine("Add another topping? Enter 'Y' or 'Yes' for yes, or press any key for no.");
                answer = Console.ReadLine().ToUpper();
                if (answer == "YES" || answer == "Y")
                {
                    Console.Clear();
                    goto Topping;
                }
            }
            else
            {
                Console.Clear();
                food[food.Count - 1].ChooseMenuPizza();
                ChooseSize();
            }
            Console.Clear();
            Console.WriteLine($"'{food[food.Count - 1].ReturnDetails["Name"]}' has been added to your order.");
            Console.WriteLine();
            Menu();
        }

        //This method allows the user to choose the size and the base of their pizza
        static void ChooseSize()
        {
            Size.ChooseBase(food[food.Count - 1].ReturnDetails["Name"], int.Parse(food[food.Count - 1].ReturnDetails["ToppingsPrice"]));
            food[food.Count - 1].ChangeDetails = Size.ReturnDetails;
            Size.ChooseSize(food[food.Count - 1].ReturnDetails["Name"], int.Parse(food[food.Count - 1].ReturnDetails["ToppingsPrice"]) + int.Parse(food[food.Count - 1].ReturnDetails["BasePrice"]));
            food[food.Count - 1].ChangeDetails = Size.ReturnDetails;
        }

        //This method allows the user to delete from their order
        static void DeleteItem()
        {
            int selection;
            string name = "";

            DisplayOrder();
            if (food.Count + drinks.Count == 0)
            {
                Console.WriteLine("You cannot delete from an empty order.");
                Console.WriteLine();
                Menu();
            }
            else
            {
                Console.WriteLine("Please enter the number of the item you wish to delete.");
                if (int.TryParse(Console.ReadLine(), out selection) && selection > 0 && selection < drinks.Count + food.Count + 1)
                {
                    if (selection > food.Count)
                    {
                        name = drinks[selection - 1 - food.Count].DrinkName;
                        drinks.Remove(drinks[selection - 1 - food.Count]);
                    }
                    else if (selection <= food.Count)
                    {
                        name = food[selection - 1].ReturnDetails["Name"];
                        food.Remove(food[selection - 1]);
                    }
                    Console.Clear();
                    Console.WriteLine($"Item number {selection}, '{name}' has been deleted.");
                    Console.WriteLine();
                    Menu();
                }
                Console.Clear();
                Console.WriteLine("Sorry, that is an invalid selection.");
                Console.WriteLine();
                DeleteItem();
            }
        }

        //This method displays the full order
        static void DisplayOrder()
        {
            int grandTotal = 0;

            if (food.Count + drinks.Count == 0)
            {
                Console.WriteLine("Your order is currently empty.");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("###########");
                Console.WriteLine("Your order:");
                Console.WriteLine("###########");
                Console.WriteLine();
                grandTotal = ViewFood();
                grandTotal += ViewDrinks();
                Console.WriteLine("#################");
                Console.WriteLine($"Grand Total: ${grandTotal}");
                Console.WriteLine("#################");
                Console.WriteLine();
            }
        }

        //This method displays the food order
        static int ViewFood()
        {
            int total = 0;

            if (food.Count > 0)
            {
                Console.WriteLine("=======");
                Console.WriteLine("Pizzas:");
                Console.WriteLine("=======");
                for (int i = 0; i < food.Count; i++)
                {
                    Console.WriteLine($"{i + 1}. {food[i].ReturnDetails["Name"]}");
                    Console.WriteLine($"({food[i].ReturnDetails["BaseName"]} - ${food[i].ReturnDetails["BasePrice"]})");
                    Console.WriteLine($"({food[i].ReturnDetails["Size"]} - ${food[i].ReturnDetails["SizePrice"]})");
                    Console.WriteLine($"(Toppings - ${food[i].ReturnDetails["ToppingsPrice"]})");
                    if (food[i].ReturnDetails["Description"] == "")
                    {
                        for (int j = 0; j < food[i].ReturnPizzaToppings.Count; j++)
                        {
                            Console.WriteLine($"    * {food[i].ReturnPizzaToppings[j].Item1} - ${food[i].ReturnPizzaToppings[j].Item2}");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"    * {food[i].ReturnDetails["Description"]}");
                    }
                    total += int.Parse(food[i].ReturnDetails["Total"]);
                    Console.WriteLine($"Total: ${food[i].ReturnDetails["Total"]}");
                    Console.WriteLine();
                }
                Console.WriteLine("--------------");
                Console.WriteLine($"Subtotal: ${total}");
                Console.WriteLine("--------------");
                Console.WriteLine();
            }
            return total;
        }

        //This method displays the drink order
        static int ViewDrinks()
        {
            int total = 0;

            if (drinks.Count > 0)
            {
                Console.WriteLine("=======");
                Console.WriteLine("Drinks:");
                Console.WriteLine("=======");
                for (int i = 0; i < drinks.Count; i++)
                {
                    Console.WriteLine($"{i + 1 + food.Count}. {drinks[i].DrinkName} - ${drinks[i].DrinkPrice}");
                    total += drinks[i].DrinkPrice;
                }
                Console.WriteLine();
                Console.WriteLine("--------------");
                Console.WriteLine($"Subtotal: ${total}");
                Console.WriteLine("--------------");
                Console.WriteLine();
            }
            return total;
        }

        //This method allows the user to enter their details and check out
        static void Checkout()
        {
            if (food.Count > 0 || drinks.Count > 0)
            {
                DisplayOrder();
                Console.WriteLine("Is this order correct? Type 'Y' or 'Yes' for yes, or type any key for no.");
                string answer = Console.ReadLine().ToUpper();
                if (answer == "Y" || answer == "YES")
                {
                    Console.Clear();
                    Client.Name();
                }
                Console.Clear();
            }
            else
            {
                Console.WriteLine("Your order is currently empty.");
                Console.WriteLine();
                Console.WriteLine("You cannot checkout with an empty order.");
                Console.WriteLine();
            }
            Menu();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_3___10004971
{
    class Client
    {
        static string name;
        static int number;

        //Allows the user to enter their name
        public static void Name()
        {
            Console.WriteLine("Please enter your name.");
            name = Console.ReadLine();
            if (name.Any(char.IsDigit) || name == "")
            {
                Console.Clear();
                Console.WriteLine("Sorry, that is an invalid selection.");
                Console.WriteLine();
                Name();
            }
            Console.Clear();
            Number();
        }

        //Allows the user to enter their number
        static void Number()
        {
            Console.WriteLine("Please enter your phone number.");
            if (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.Clear();
                Console.WriteLine("Sorry, that is an invalid selection.");
                Console.WriteLine();
                Number();
            }
            Console.Clear();
            Confirm();
        }

        //Confirms the user's details and checks out
        static void Confirm()
        {
            Console.WriteLine($"Name: {name}");
            Console.WriteLine($"Phone Number: {number}");
            Console.WriteLine("Are these details correct? Type 'Y' for yes, or type any key for no.");
            string answer = Console.ReadLine().ToUpper();
            if (answer == "Y" || answer == "YES")
            {
                Console.Clear();
                Console.WriteLine("Your credit card has now been charged and your order has been placed.");
                Console.WriteLine("Thank you for your order.");
                Environment.Exit(0);
            }
            else
            {
                Console.Clear();
                Name();
            }
        }
    }
}